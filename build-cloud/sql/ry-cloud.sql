/*
 Navicat Premium Data Transfer

 Source Server         : 167
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.0.167:3306
 Source Schema         : ry-cloud

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 06/08/2021 16:00:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'sys_dept', '部门表', 'SysDept', 'crud', 'com.ruoyi.system', 'system', 'dept', '部门', 'ruoyi', '0', '/', NULL, 'admin', '2020-12-23 16:22:44', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'dept_id', '部门id', 'bigint(20)', 'Long', 'deptId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (2, '1', 'parent_id', '父部门id', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (3, '1', 'ancestors', '祖级列表', 'varchar(50)', 'String', 'ancestors', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (4, '1', 'dept_name', '部门名称', 'varchar(30)', 'String', 'deptName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (5, '1', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (6, '1', 'leader', '负责人', 'varchar(20)', 'String', 'leader', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (7, '1', 'phone', '联系电话', 'varchar(11)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (8, '1', 'email', '邮箱', 'varchar(50)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (9, '1', 'status', '部门状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 9, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (10, '1', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (12, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (13, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2020-12-23 16:22:44', '', NULL);
INSERT INTO `gen_table_column` VALUES (14, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2020-12-23 16:22:44', '', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'master1613816919404', 1614062904079, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1613816980000, -1, 5, 'PAUSED', 'CRON', 1613816977000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1613816985000, -1, 5, 'PAUSED', 'CRON', 1613816979000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1613817000000, -1, 5, 'PAUSED', 'CRON', 1613816982000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深色主题theme-dark，浅色主题theme-light');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 1, '授权码模式', 'authorization_code', 'sys_grant_type', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权码模式');
INSERT INTO `sys_dict_data` VALUES (30, 2, '密码模式', 'password', 'sys_grant_type', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '密码模式');
INSERT INTO `sys_dict_data` VALUES (31, 3, '客户端模式', 'client_credentials', 'sys_grant_type', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '客户端模式');
INSERT INTO `sys_dict_data` VALUES (32, 4, '简化模式', 'implicit', 'sys_grant_type', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '简化模式');
INSERT INTO `sys_dict_data` VALUES (33, 5, '刷新模式', 'refresh_token', 'sys_grant_type', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '刷新模式');
INSERT INTO `sys_dict_data` VALUES (100, 0, 'JAVA', '1', 'gen_type', NULL, NULL, 'N', '0', 'admin', '2020-06-19 13:07:47', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 0, 'XML文件', '2', 'gen_type', NULL, NULL, 'N', '0', 'admin', '2020-06-19 13:08:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 0, 'VUE文件', '3', 'gen_type', NULL, NULL, 'N', '0', 'admin', '2020-06-19 13:08:11', 'admin', '2020-08-26 11:35:01', '11');
INSERT INTO `sys_dict_data` VALUES (103, 0, 'JS文件', '4', 'gen_type', NULL, NULL, 'N', '0', 'admin', '2020-06-19 13:08:20', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 0, 'sql', '5', 'gen_type', NULL, NULL, 'N', '0', 'admin', '2020-06-19 13:50:47', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '授权类型', 'sys_grant_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权类型列表');
INSERT INTO `sys_dict_type` VALUES (100, '代码生成类型', 'gen_type', '0', 'admin', '2020-06-19 13:07:38', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '172.31.0.6', '0', '退出成功', '2020-12-23 17:30:38');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '172.31.0.6', '0', '登录成功', '2020-12-23 17:30:47');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '172.31.0.6', '0', '登录成功', '2020-12-24 09:37:34');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '172.31.0.6', '1', '用户密码错误', '2020-12-24 10:31:34');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '172.31.0.6', '1', '用户密码错误', '2020-12-24 10:31:40');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '172.31.0.6', '0', '登录成功', '2020-12-24 10:31:51');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '192.168.0.222', '0', '退出成功', '2020-12-24 11:18:31');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-24 11:31:24');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-24 14:43:31');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-24 14:43:38');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-24 16:20:34');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-24 16:20:39');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-24 16:20:48');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-24 16:21:00');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-24 17:58:06');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-25 10:17:52');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-25 10:17:55');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-25 10:18:10');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-25 15:21:10');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-25 15:21:14');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-25 15:21:23');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-28 09:58:37');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '192.168.0.222', '1', '用户密码错误', '2020-12-28 09:58:46');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-28 09:58:54');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-28 10:55:06');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-28 11:40:09');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-28 16:32:58');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-28 16:33:57');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-29 09:59:35');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '192.168.0.222', '0', '登录成功', '2020-12-29 14:39:17');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '192.168.0.222', '0', '登录成功', '2021-01-08 09:40:36');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '192.168.0.222', '0', '登录成功', '2021-01-08 09:40:47');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '192.168.0.222', '0', '登录成功', '2021-01-11 17:52:01');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '192.168.0.222', '0', '登录成功', '2021-01-13 17:32:10');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-20 15:51:30');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-20 16:01:06');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '192.168.50.222', '1', '用户密码错误', '2021-01-21 10:40:33');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-21 10:40:42');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-27 09:44:09');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-27 09:44:16');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-27 14:17:22');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-28 16:17:37');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-28 16:17:37');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-28 16:17:37');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-28 16:17:38');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-28 16:17:53');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '192.168.50.222', '0', '登录成功', '2021-01-29 17:07:20');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-01 15:10:48');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-01 15:58:00');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '192.168.50.222', '1', '用户密码错误', '2021-02-02 11:46:21');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-02 11:46:30');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '192.168.50.222', '0', '退出成功', '2021-02-02 15:36:53');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-02 17:43:07');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-03 10:58:26');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '192.168.50.222', '0', '退出成功', '2021-02-03 17:55:41');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-03 17:56:53');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-04 10:09:44');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-04 10:09:46');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-04 10:18:57');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-05 09:45:50');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-05 09:46:00');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-05 14:42:17');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-05 14:42:18');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-05 16:01:57');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-22 09:32:26');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-22 09:32:26');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '192.168.50.222', '0', '登录成功', '2021-02-22 09:33:06');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2024 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 9, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 10, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', 1, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://192.168.0.222:7002/nacos', '', 1, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', '2018-03-16 11:33:00', 'admin', '2020-12-28 17:12:57', '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://192.168.0.222:9208/login', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2018-03-16 11:33:00', 'admin', '2020-12-28 17:13:38', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://192.168.0.222:8081/doc.html', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2018-03-16 11:33:00', 'admin', '2020-12-28 17:12:38', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', 1, 0, 'C', '0', '0', 'system:operlog:list', 'form', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', 1, 0, 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1060, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1061, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1062, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1063, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1064, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1065, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (2000, '测试用例', 3, 1, 'case', 'gen/case/index', 1, 0, 'C', '0', '0', 'gen:case:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '测试用例菜单');
INSERT INTO `sys_menu` VALUES (2001, '测试用例查询', 2000, 1, '#', '', 1, 0, 'F', '0', '0', 'gen:case:query', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2002, '测试用例新增', 2000, 2, '#', '', 1, 0, 'F', '0', '0', 'gen:case:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2003, '测试用例修改', 2000, 3, '#', '', 1, 0, 'F', '0', '0', 'gen:case:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2004, '测试用例删除', 2000, 4, '#', '', 1, 0, 'F', '0', '0', 'gen:case:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2005, '测试用例导出', 2000, 5, '#', '', 1, 0, 'F', '0', '0', 'gen:case:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2006, '代码生成模板组管理', 3, 1, 'scheme', 'gen/scheme/index', 1, 0, 'C', '0', '0', 'gen:scheme:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '代码生成模板组管理菜单');
INSERT INTO `sys_menu` VALUES (2007, '代码生成模板组管理查询', 2006, 1, '#', '', 1, 0, 'F', '0', '0', 'gen:scheme:query', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2008, '代码生成模板组管理新增', 2006, 2, '#', '', 1, 0, 'F', '0', '0', 'gen:scheme:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2009, '代码生成模板组管理修改', 2006, 3, '#', '', 1, 0, 'F', '0', '0', 'gen:scheme:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2010, '代码生成模板组管理删除', 2006, 4, '#', '', 1, 0, 'F', '0', '0', 'gen:scheme:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2011, '代码生成模板组管理导出', 2006, 5, '#', '', 1, 0, 'F', '0', '0', 'gen:scheme:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2012, '数据源', 3, 1, 'source', 'gen/source/index', 1, 0, 'C', '0', '0', 'gen:source:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '数据源菜单');
INSERT INTO `sys_menu` VALUES (2013, '数据源查询', 2012, 1, '#', '', 1, 0, 'F', '0', '0', 'gen:source:query', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2014, '数据源新增', 2012, 2, '#', '', 1, 0, 'F', '0', '0', 'gen:source:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2015, '数据源修改', 2012, 3, '#', '', 1, 0, 'F', '0', '0', 'gen:source:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2016, '数据源删除', 2012, 4, '#', '', 1, 0, 'F', '0', '0', 'gen:source:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2017, '数据源导出', 2012, 5, '#', '', 1, 0, 'F', '0', '0', 'gen:source:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2018, '代码生成模板管理1', 3, 1, 'template', 'gen/template/index', 1, 0, 'C', '0', '0', 'gen:template:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '代码生成模板管理1菜单');
INSERT INTO `sys_menu` VALUES (2019, '代码生成模板管理1查询', 2018, 1, '#', '', 1, 0, 'F', '0', '0', 'gen:template:query', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2020, '代码生成模板管理1新增', 2018, 2, '#', '', 1, 0, 'F', '0', '0', 'gen:template:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2021, '代码生成模板管理1修改', 2018, 3, '#', '', 1, 0, 'F', '0', '0', 'gen:template:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2022, '代码生成模板管理1删除', 2018, 4, '#', '', 1, 0, 'F', '0', '0', 'gen:template:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (2023, '代码生成模板管理1导出', 2018, 5, '#', '', 1, 0, 'F', '0', '0', 'gen:template:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 215 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', 'sys_dept', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 16:22:44');
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-23 16:22:46');
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-24 14:49:34');
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-24 14:57:00');
INSERT INTO `sys_oper_log` VALUES (104, '数据源', 1, 'com.ruoyi.gen.controller.GeneratorDataSourceController.add()', 'POST', 1, 'admin', NULL, '/source', '127.0.0.1', '', '{\"dbName\":\"sy\",\"description\":\"若依微服务\",\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8\\\" +                 \\\"&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":1342305729310228481,\"dbPassword\":\"123456\"}', 'null', 1, '\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'id\' at row 1\n### The error may exist in com/ruoyi/gen/mapper/GeneratorDataSourceMapper.java (best guess)\n### The error may involve com.ruoyi.gen.mapper.GeneratorDataSourceMapper.insert-Inline\n### The error occurred while setting parameters\n### SQL: INSERT INTO generator_data_source  ( id, db_key, description, driver_class, url, db_user, db_password,  db_name )  VALUES  ( ?, ?, ?, ?, ?, ?, ?,  ? )\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'id\' at row 1\n; Data truncation: Out of range value for column \'id\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'id\' at row 1', '2020-12-25 11:06:40');
INSERT INTO `sys_oper_log` VALUES (105, '数据源', 1, 'com.ruoyi.gen.controller.GeneratorDataSourceController.add()', 'POST', 1, 'admin', NULL, '/source', '127.0.0.1', '', '{\"dbName\":\"sy\",\"description\":\"若依微服务\",\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8\\\" +                 \\\"&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":2,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"sy\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"若依微服务\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":2,\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8\\\" +                 \\\"&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-25 11:07:07');
INSERT INTO `sys_oper_log` VALUES (106, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '127.0.0.1', '', '{\"dbName\":\"sy\",\"description\":\"若依微服务\",\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":2,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"sy\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"若依微服务\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":2,\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-25 14:30:45');
INSERT INTO `sys_oper_log` VALUES (107, '数据源', 3, 'com.ruoyi.gen.controller.GeneratorDataSourceController.remove()', 'DELETE', 1, 'admin', NULL, '/source/1', '127.0.0.1', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-25 14:30:48');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 14:32:17');
INSERT INTO `sys_oper_log` VALUES (109, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"appointment\"}', 'null', 1, '[-40001]导入表请求参数错误，参数表名不能为空！', '2020-12-25 15:16:33');
INSERT INTO `sys_oper_log` VALUES (110, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"appointment\"}', 'null', 1, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'?)\' at line 1', '2020-12-25 16:05:01');
INSERT INTO `sys_oper_log` VALUES (111, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"appointment\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:39:32');
INSERT INTO `sys_oper_log` VALUES (112, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"appointment\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:11:40');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"con_invoice\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:12:41');
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '127.0.0.1', '', '{\"sourceId\":2,\"tables\":\"con_manage_record\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:13:10');
INSERT INTO `sys_oper_log` VALUES (115, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '127.0.0.1', '', '{\"dbName\":\"sy\",\"description\":\"长航\",\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":2,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"sy\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"长航\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":2,\"url\":\"jdbc:mysql://192.168.0.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-25 17:13:22');
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-25 18:05:12');
INSERT INTO `sys_oper_log` VALUES (117, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-28 10:01:39');
INSERT INTO `sys_oper_log` VALUES (118, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-28 10:01:52');
INSERT INTO `sys_oper_log` VALUES (119, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 1, 'For input string: \"crud\"', '2020-12-28 10:05:44');
INSERT INTO `sys_oper_log` VALUES (120, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-28 10:07:07');
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-28 11:35:54');
INSERT INTO `sys_oper_log` VALUES (122, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '127.0.0.1', '', NULL, 'null', 0, NULL, '2020-12-28 14:50:02');
INSERT INTO `sys_oper_log` VALUES (123, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"胡琴\",\"parentTask\":\"110\",\"module\":\"测试\",\"priority\":\"重要\",\"type\":\"110\",\"solveResults\":\"开始\",\"theme\":\"110\",\"id\":131,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"expectedCompletionTime\":1609084800000,\"id\":131,\"module\":\"测试\",\"parentTask\":\"110\",\"priority\":\"重要\",\"solveResults\":\"开始\",\"status\":\"0\",\"theme\":\"110\",\"type\":\"110\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:36:32');
INSERT INTO `sys_oper_log` VALUES (124, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"胡琴\",\"parentTask\":\"110\",\"module\":\"测试\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"110\",\"solveResults\":\"开始\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"110\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609084800000,\"id\":131,\"module\":\"测试\",\"parentTask\":\"110\",\"priority\":\"重要\",\"solveResults\":\"开始\",\"status\":\"1\",\"theme\":\"110\",\"type\":\"110\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:36:54');
INSERT INTO `sys_oper_log` VALUES (125, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"胡琴\",\"parentTask\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"110\",\"label\":\"110\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"110\",\"solveResults\":\"开始\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"110\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609084800000,\"id\":131,\"label\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"110\",\"parentTask\":\"110\",\"priority\":\"重要\",\"solveResults\":\"开始\",\"status\":\"1\",\"theme\":\"110\",\"type\":\"110\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:37:04');
INSERT INTO `sys_oper_log` VALUES (126, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609344000000,\"agent\":\"胡琴\",\"parentTask\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"label\":\"110\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"110\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"110\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609344000000,\"id\":131,\"label\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"110\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"1\",\"theme\":\"110\",\"type\":\"110\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:38:04');
INSERT INTO `sys_oper_log` VALUES (127, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609344000000,\"agent\":\"胡琴001\",\"parentTask\":\"110001\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"label\":\"110\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"110001\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"110001\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴001\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609344000000,\"id\":131,\"label\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"110001\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"1\",\"theme\":\"110001\",\"type\":\"110001\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:39:34');
INSERT INTO `sys_oper_log` VALUES (128, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609344000000,\"agent\":\"胡琴\",\"parentTask\":\"测试01\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"label\":\"110\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"bug\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609344000000,\"id\":131,\"label\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"测试01\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"1\",\"theme\":\"测试\",\"type\":\"bug\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:42:12');
INSERT INTO `sys_oper_log` VALUES (129, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609344000000,\"agent\":\"胡琴\",\"parentTask\":\"测试01\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"label\":\"110\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试\",\"id\":131,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609344000000,\"id\":131,\"label\":\"110\",\"module\":\"测试\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"测试01\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"1\",\"theme\":\"测试\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:42:23');
INSERT INTO `sys_oper_log` VALUES (130, '测试用例', 3, 'com.ruoyi.gen.controller.TestCaseController.remove()', 'DELETE', 1, 'admin', NULL, '/case/130', '192.168.0.147', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-28 16:42:53');
INSERT INTO `sys_oper_log` VALUES (131, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"胡琴\",\"parentTask\":\"测试01\",\"module\":\"前端\",\"originalEstimateTime\":\"1h\",\"label\":\"222\",\"priority\":\"重要\",\"type\":\"新功能\",\"solveResults\":\"为解决\",\"theme\":\"测试02\",\"id\":132,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"expectedCompletionTime\":1609084800000,\"id\":132,\"label\":\"222\",\"module\":\"前端\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"测试01\",\"priority\":\"重要\",\"solveResults\":\"为解决\",\"status\":\"1\",\"theme\":\"测试02\",\"type\":\"新功能\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:43:51');
INSERT INTO `sys_oper_log` VALUES (132, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"小胡鸭\",\"parentTask\":\"测试01\",\"module\":\"前端\",\"originalEstimateTime\":\"1h\",\"label\":\"222\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"新功能\",\"solveResults\":\"为解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试02\",\"id\":132,\"status\":\"1\"}', '{\"code\":200,\"data\":{\"agent\":\"小胡鸭\",\"createBy\":\"\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609084800000,\"id\":132,\"label\":\"222\",\"module\":\"前端\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"测试01\",\"priority\":\"重要\",\"solveResults\":\"为解决\",\"status\":\"1\",\"theme\":\"测试02\",\"type\":\"新功能\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:44:23');
INSERT INTO `sys_oper_log` VALUES (133, '代码生成模板组管理', 1, 'com.ruoyi.gen.controller.GenTemplateSchemeController.add()', 'POST', 1, 'admin', NULL, '/scheme', '192.168.0.147', '', '{\"description\":\"进行数据更新等等\",\"id\":8,\"title\":\"增删改查\"}', '{\"code\":200,\"data\":{\"description\":\"进行数据更新等等\",\"id\":8,\"title\":\"增删改查\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:48:33');
INSERT INTO `sys_oper_log` VALUES (134, '代码生成模板组管理', 3, 'com.ruoyi.gen.controller.GenTemplateSchemeController.remove()', 'DELETE', 1, 'admin', NULL, '/scheme/2,7', '192.168.0.147', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-28 16:48:54');
INSERT INTO `sys_oper_log` VALUES (135, '代码生成模板组管理', 2, 'com.ruoyi.gen.controller.GenTemplateSchemeController.edit()', 'PUT', 1, 'admin', NULL, '/scheme', '192.168.0.147', '', '{\"description\":\"进行数据更新等等\",\"id\":8,\"title\":\"增删改查001\"}', '{\"code\":200,\"data\":{\"description\":\"进行数据更新等等\",\"id\":8,\"title\":\"增删改查001\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:49:00');
INSERT INTO `sys_oper_log` VALUES (136, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{}', 'null', 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\n### The error occurred while setting parameters\n### SQL: INSERT INTO gen_template    VALUES\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2020-12-28 16:53:39');
INSERT INTO `sys_oper_log` VALUES (137, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\r\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO gen_template    VALUES\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2020-12-28 16:53:41');
INSERT INTO `sys_oper_log` VALUES (138, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"id\":133,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"id\":133,\"status\":\"0\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:53:46');
INSERT INTO `sys_oper_log` VALUES (139, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.147', '', '{\"id\":134,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"id\":134,\"status\":\"0\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:53:55');
INSERT INTO `sys_oper_log` VALUES (140, '测试用例', 3, 'com.ruoyi.gen.controller.TestCaseController.remove()', 'DELETE', 1, 'admin', NULL, '/case/134', '192.168.0.147', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-28 16:53:58');
INSERT INTO `sys_oper_log` VALUES (141, '数据源', 1, 'com.ruoyi.gen.controller.GeneratorDataSourceController.add()', 'POST', 1, 'admin', NULL, '/source', '192.168.0.147', '', '{\"dbName\":\"hlf\",\"description\":\"换流阀\",\"url\":\"127.0.0.1\",\"dbKey\":\"hlf\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"admin\",\"id\":5,\"remarks\":\"1111\",\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"hlf\",\"dbName\":\"hlf\",\"dbPassword\":\"123456\",\"dbUser\":\"admin\",\"description\":\"换流阀\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":5,\"remarks\":\"1111\",\"url\":\"127.0.0.1\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:57:25');
INSERT INTO `sys_oper_log` VALUES (142, '数据源', 1, 'com.ruoyi.gen.controller.GeneratorDataSourceController.add()', 'POST', 1, 'admin', NULL, '/source', '192.168.0.147', '', '{\"dbName\":\"mzk\",\"description\":\"媒资裤\",\"url\":\"192.168.0.119\",\"dbKey\":\"mzk\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"admin123\",\"id\":6,\"remarks\":\"mzk\",\"dbPassword\":\"123\"}', '{\"code\":200,\"data\":{\"dbKey\":\"mzk\",\"dbName\":\"mzk\",\"dbPassword\":\"123\",\"dbUser\":\"admin123\",\"description\":\"媒资裤\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":6,\"remarks\":\"mzk\",\"url\":\"192.168.0.119\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:59:14');
INSERT INTO `sys_oper_log` VALUES (143, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '192.168.0.147', '', '{\"dbName\":\"hlf\",\"description\":\"换流阀\",\"url\":\"127.0.0.1\",\"dbKey\":\"hlf\",\"driverClass\":\"com.mysql.cj\",\"dbUser\":\"admin\",\"id\":5,\"remarks\":\"1111\",\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"hlf\",\"dbName\":\"hlf\",\"dbPassword\":\"123456\",\"dbUser\":\"admin\",\"description\":\"换流阀\",\"driverClass\":\"com.mysql.cj\",\"id\":5,\"remarks\":\"1111\",\"url\":\"127.0.0.1\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 16:59:58');
INSERT INTO `sys_oper_log` VALUES (144, '数据源', 3, 'com.ruoyi.gen.controller.GeneratorDataSourceController.remove()', 'DELETE', 1, 'admin', NULL, '/source/6', '192.168.0.147', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-28 17:02:03');
INSERT INTO `sys_oper_log` VALUES (145, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"1\",\"name\":\"page\",\"schemeId\":1,\"targetPackage\":\"page/manager\"}', 'null', 1, '\n### Error updating database.  Cause: java.sql.SQLException: Field \'template_content\' doesn\'t have a default value\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\n### The error occurred while setting parameters\n### SQL: INSERT INTO gen_template  ( name,  target_package,  scheme_id,  template_type )  VALUES  ( ?,  ?,  ?,  ? )\n### Cause: java.sql.SQLException: Field \'template_content\' doesn\'t have a default value\n; Field \'template_content\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'template_content\' doesn\'t have a default value', '2020-12-28 17:05:12');
INSERT INTO `sys_oper_log` VALUES (146, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"1\",\"name\":\"page\",\"templateContent\":\"1111111111111\",\"schemeId\":1,\"id\":39,\"targetPackage\":\"page/manager\"}', '{\"code\":200,\"data\":{\"id\":39,\"name\":\"page\",\"schemeId\":1,\"targetPackage\":\"page/manager\",\"templateContent\":\"1111111111111\",\"templateType\":\"1\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:05:17');
INSERT INTO `sys_oper_log` VALUES (147, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"1\",\"name\":\"page\",\"templateContent\":\"\",\"schemeId\":1,\"id\":39,\"delFlag\":\"0\",\"targetPackage\":\"page/manager\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"id\":39,\"name\":\"page\",\"schemeId\":1,\"targetPackage\":\"page/manager\",\"templateContent\":\"\",\"templateType\":\"1\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:05:27');
INSERT INTO `sys_oper_log` VALUES (148, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"1\",\"name\":\"page\",\"templateContent\":\"\",\"schemeId\":1,\"id\":39,\"delFlag\":\"0\",\"targetPackage\":\"page/manager\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"id\":39,\"name\":\"page\",\"schemeId\":1,\"targetPackage\":\"page/manager\",\"templateContent\":\"\",\"templateType\":\"1\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:05:36');
INSERT INTO `sys_oper_log` VALUES (149, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"3\",\"name\":\"page\",\"templateContent\":\"\",\"schemeId\":1,\"id\":39,\"delFlag\":\"0\",\"targetPackage\":\"page/manager\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"id\":39,\"name\":\"page\",\"schemeId\":1,\"targetPackage\":\"page/manager\",\"templateContent\":\"\",\"templateType\":\"3\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:05:44');
INSERT INTO `sys_oper_log` VALUES (150, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"2\",\"fileName\":\"show\",\"name\":\"110\",\"schemeId\":1,\"targetPackage\":\"devices/show\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'template_content\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\r\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO gen_template  ( name, file_name, target_package,  scheme_id,  template_type )  VALUES  ( ?, ?, ?,  ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'template_content\' doesn\'t have a default value\n; Field \'template_content\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'template_content\' doesn\'t have a default value', '2020-12-28 17:06:22');
INSERT INTO `sys_oper_log` VALUES (151, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"2\",\"fileName\":\"show\",\"name\":\"110\",\"templateContent\":\"111111111\",\"schemeId\":1,\"id\":40,\"targetPackage\":\"devices/show\"}', '{\"code\":200,\"data\":{\"fileName\":\"show\",\"id\":40,\"name\":\"110\",\"schemeId\":1,\"targetPackage\":\"devices/show\",\"templateContent\":\"111111111\",\"templateType\":\"2\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:06:41');
INSERT INTO `sys_oper_log` VALUES (152, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"2\",\"fileName\":\"show\",\"name\":\"js\",\"templateContent\":\"111111111\",\"schemeId\":1,\"id\":40,\"delFlag\":\"0\",\"targetPackage\":\"devices/show\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"fileName\":\"show\",\"id\":40,\"name\":\"js\",\"schemeId\":1,\"targetPackage\":\"devices/show\",\"templateContent\":\"111111111\",\"templateType\":\"2\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:07:00');
INSERT INTO `sys_oper_log` VALUES (153, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"2\",\"fileName\":\"show\",\"name\":\"js\",\"templateContent\":\"111111111\",\"schemeId\":1,\"id\":40,\"delFlag\":\"0\",\"targetPackage\":\"devices/show\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"fileName\":\"show\",\"id\":40,\"name\":\"js\",\"schemeId\":1,\"targetPackage\":\"devices/show\",\"templateContent\":\"111111111\",\"templateType\":\"2\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:07:43');
INSERT INTO `sys_oper_log` VALUES (154, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"3\",\"createBy\":\"admin\",\"fileName\":\"{}data\",\"createTime\":1592798191000,\"name\":\"vue数据处理\",\"templateContent\":\"  form: {\\n                #foreach ($column in $columns)\\n\\n                    $column.javaField: null,//$column.columnComment\\n                #end\\n            },\",\"schemeId\":1,\"id\":32,\"delFlag\":\"0\",\"targetPackage\":\"js\"}', '{\"code\":200,\"data\":{\"createBy\":\"admin\",\"createTime\":1592798191000,\"delFlag\":\"0\",\"fileName\":\"{}data\",\"id\":32,\"name\":\"vue数据处理\",\"schemeId\":1,\"targetPackage\":\"js\",\"templateContent\":\"  form: {\\n                #foreach ($column in $columns)\\n\\n                    $column.javaField: null,//$column.columnComment\\n                #end\\n            },\",\"templateType\":\"3\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:07:50');
INSERT INTO `sys_oper_log` VALUES (155, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"5\",\"fileName\":\"show\",\"name\":\"js\",\"templateContent\":\"111111111\",\"schemeId\":1,\"id\":40,\"delFlag\":\"0\",\"targetPackage\":\"devices/show\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"fileName\":\"show\",\"id\":40,\"name\":\"js\",\"schemeId\":1,\"targetPackage\":\"devices/show\",\"templateContent\":\"111111111\",\"templateType\":\"5\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:08:22');
INSERT INTO `sys_oper_log` VALUES (156, '代码生成模板管理1', 2, 'com.ruoyi.gen.controller.GenTemplateController.edit()', 'PUT', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{\"templateType\":\"5\",\"fileName\":\"show\",\"name\":\"java\",\"templateContent\":\"111111111\",\"schemeId\":1,\"id\":40,\"delFlag\":\"0\",\"targetPackage\":\"devices/show\"}', '{\"code\":200,\"data\":{\"delFlag\":\"0\",\"fileName\":\"show\",\"id\":40,\"name\":\"java\",\"schemeId\":1,\"targetPackage\":\"devices/show\",\"templateContent\":\"111111111\",\"templateType\":\"5\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-28 17:10:19');
INSERT INTO `sys_oper_log` VALUES (157, '代码生成模板管理1', 3, 'com.ruoyi.gen.controller.GenTemplateController.remove()', 'DELETE', 1, 'admin', NULL, '/template/40', '192.168.0.147', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-28 17:10:33');
INSERT INTO `sys_oper_log` VALUES (158, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:12:32');
INSERT INTO `sys_oper_log` VALUES (159, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:12:35');
INSERT INTO `sys_oper_log` VALUES (160, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"swagger\",\"orderNum\":\"3\",\"menuName\":\"系统接口\",\"params\":{},\"parentId\":3,\"isCache\":\"0\",\"path\":\"http://192.168.0.222:8081/doc.html\",\"component\":\"\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":116,\"menuType\":\"C\",\"perms\":\"tool:swagger:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:12:39');
INSERT INTO `sys_oper_log` VALUES (161, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"nacos\",\"orderNum\":\"4\",\"menuName\":\"Nacos控制台\",\"params\":{},\"parentId\":2,\"isCache\":\"0\",\"path\":\"http://192.168.0.222:7002/nacos\",\"component\":\"\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":112,\"menuType\":\"C\",\"perms\":\"monitor:nacos:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:12:57');
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '127.0.0.1', '', '{\"visible\":\"0\",\"icon\":\"server\",\"orderNum\":\"5\",\"menuName\":\"Admin控制台\",\"params\":{},\"parentId\":2,\"isCache\":\"0\",\"path\":\"http://192.168.0.222:9208/login\",\"component\":\"\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":113,\"menuType\":\"C\",\"perms\":\"monitor:server:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:13:39');
INSERT INTO `sys_oper_log` VALUES (163, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '192.168.0.147', '', '{\"sourceId\":2,\"tables\":\"appointment\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:16:16');
INSERT INTO `sys_oper_log` VALUES (164, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '192.168.0.147', '', '{\"sourceId\":2,\"tables\":\"base_table\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:17:40');
INSERT INTO `sys_oper_log` VALUES (165, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSaveNew()', 'PUT', 1, 'admin', NULL, '/gen/importTable', '192.168.0.147', '', '{\"sourceId\":2,\"tables\":\"con_invoice_customer,base_table,appointment\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:17:59');
INSERT INTO `sys_oper_log` VALUES (166, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2020-12-28 17:25:02');
INSERT INTO `sys_oper_log` VALUES (167, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2020-12-28 17:25:04');
INSERT INTO `sys_oper_log` VALUES (168, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2020-12-28 17:25:13');
INSERT INTO `sys_oper_log` VALUES (169, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2020-12-28 17:25:17');
INSERT INTO `sys_oper_log` VALUES (170, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:25:18');
INSERT INTO `sys_oper_log` VALUES (171, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:25:20');
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:27:30');
INSERT INTO `sys_oper_log` VALUES (173, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 1, 'nested exception is org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2', '2020-12-28 17:28:28');
INSERT INTO `sys_oper_log` VALUES (174, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '192.168.0.147', '', NULL, 'null', 0, NULL, '2020-12-28 17:28:41');
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 3, 'com.ruoyi.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/12', '192.168.0.147', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:29:49');
INSERT INTO `sys_oper_log` VALUES (176, '代码生成', 3, 'com.ruoyi.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/11', '192.168.0.147', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 17:30:54');
INSERT INTO `sys_oper_log` VALUES (177, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\r\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO gen_template    VALUES\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2020-12-28 17:33:59');
INSERT INTO `sys_oper_log` VALUES (178, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{}', 'null', 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\n### The error occurred while setting parameters\n### SQL: INSERT INTO gen_template    VALUES\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2020-12-28 17:35:30');
INSERT INTO `sys_oper_log` VALUES (179, '代码生成模板管理1', 1, 'com.ruoyi.gen.controller.GenTemplateController.add()', 'POST', 1, 'admin', NULL, '/template', '192.168.0.147', '', '{}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in com/ruoyi/gen/mapper/GenTemplateMapper.java (best guess)\r\n### The error may involve com.ruoyi.gen.mapper.GenTemplateMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO gen_template    VALUES\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2020-12-28 17:36:37');
INSERT INTO `sys_oper_log` VALUES (180, '角色管理', 2, 'com.ruoyi.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"超级管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createTime\":1521171180000,\"menuCheckStrictly\":true,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员角色', '2020-12-28 18:00:31');
INSERT INTO `sys_oper_log` VALUES (181, '角色管理', 2, 'com.ruoyi.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1521171180000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050,2,109,1051,1052,1053,110,1054,1055,1056,1057,1058,1059,111,112,113,3,114,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,115,1060,1061,1063,1062,1064,1065,116,4],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 18:00:44');
INSERT INTO `sys_oper_log` VALUES (182, '角色管理', 2, 'com.ruoyi.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"超级管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createTime\":1521171180000,\"menuCheckStrictly\":true,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050],\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员角色', '2020-12-28 18:00:50');
INSERT INTO `sys_oper_log` VALUES (183, '角色管理', 2, 'com.ruoyi.system.controller.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/role', '127.0.0.1', '', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1521171180000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[3,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1041,1042,1043,1044,108,500,1045,1046,1047,501,1048,1049,1050,2,109,1051,1052,1053,110,1054,1055,1056,1057,1058,1059,111,112,113,114,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2018,2019,2020,2021,2022,2023,115,1060,1061,1063,1062,1064,1065,116,4],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 18:02:16');
INSERT INTO `sys_oper_log` VALUES (184, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://127.0.0.1:9300/statics/2020/12/29/b1113232-58e2-48e9-babe-2166372e20fb.jpeg\",\"code\":200}', 0, NULL, '2020-12-29 15:21:16');
INSERT INTO `sys_oper_log` VALUES (185, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://127.0.0.1:9300/statics/2020/12/29/92604f99-c770-47b7-a6d4-b06ee64a651f.jpeg\",\"code\":200}', 0, NULL, '2020-12-29 15:24:55');
INSERT INTO `sys_oper_log` VALUES (186, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://127.0.0.1:9300/statics/2020/12/29/62264ece-2445-437d-a91a-db9ab93f1371.jpeg\",\"code\":200}', 0, NULL, '2020-12-29 15:25:00');
INSERT INTO `sys_oper_log` VALUES (187, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"文件服务异常，请联系管理员\",\"code\":500}', 0, NULL, '2020-12-29 15:47:05');
INSERT INTO `sys_oper_log` VALUES (188, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"文件服务异常，请联系管理员\",\"code\":500}', 0, NULL, '2020-12-29 16:08:39');
INSERT INTO `sys_oper_log` VALUES (189, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://192.168.0.222:4000/test/2020/12/29/0d9d9367-0ce8-4f79-b426-4499c2a7b867.jpeg\",\"code\":200}', 0, NULL, '2020-12-29 16:32:37');
INSERT INTO `sys_oper_log` VALUES (190, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.0.140', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://192.168.0.222:4000/test/2020/12/29/60b552a4-b2b9-4422-b406-568e61ce8690.jpeg\",\"code\":200}', 0, NULL, '2020-12-29 16:34:23');
INSERT INTO `sys_oper_log` VALUES (191, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"胡琴\",\"parentTask\":\"1\",\"originalEstimateTime\":\"1h\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试用例-水印信息管理的界面查询\",\"id\":100,\"status\":\"开始\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"id\":100,\"originalEstimateTime\":\"1h\",\"parentTask\":\"1\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"开始\",\"theme\":\"测试用例-水印信息管理的界面查询\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:12:36');
INSERT INTO `sys_oper_log` VALUES (192, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"expectedCompletionTime\":1609084800000,\"agent\":\"213\",\"parentTask\":\"213\",\"module\":\"213\",\"originalEstimateTime\":\"312\",\"label\":\"123\",\"priority\":\"213\",\"type\":\"123\",\"solveResults\":\"321\",\"theme\":\"123\",\"id\":134,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"agent\":\"213\",\"expectedCompletionTime\":1609084800000,\"id\":134,\"label\":\"123\",\"module\":\"213\",\"originalEstimateTime\":\"312\",\"parentTask\":\"213\",\"priority\":\"213\",\"solveResults\":\"321\",\"status\":\"0\",\"theme\":\"123\",\"type\":\"123\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:13:04');
INSERT INTO `sys_oper_log` VALUES (193, '测试用例', 3, 'com.ruoyi.gen.controller.TestCaseController.remove()', 'DELETE', 1, 'admin', NULL, '/case/133', '192.168.0.140', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2020-12-29 17:13:54');
INSERT INTO `sys_oper_log` VALUES (194, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"胡琴\",\"parentTask\":\"1\",\"module\":\"111\",\"originalEstimateTime\":\"1h\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试用例-水印信息管理的界面查询\",\"id\":100,\"status\":\"开始\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"id\":100,\"module\":\"111\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"1\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"开始\",\"theme\":\"测试用例-水印信息管理的界面查询\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:22:47');
INSERT INTO `sys_oper_log` VALUES (195, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"expectedCompletionTime\":1609171200000,\"agent\":\"123\",\"parentTask\":\"123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"label\":\"123\",\"priority\":\"123\",\"type\":\"123\",\"solveResults\":\"123\",\"createBy\":\"admin\",\"theme\":\"123\",\"id\":135,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"agent\":\"123\",\"createBy\":\"admin\",\"expectedCompletionTime\":1609171200000,\"id\":135,\"label\":\"123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"parentTask\":\"123\",\"priority\":\"123\",\"solveResults\":\"123\",\"status\":\"0\",\"theme\":\"123\",\"type\":\"123\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:24:58');
INSERT INTO `sys_oper_log` VALUES (196, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"expectedCompletionTime\":1609171200000,\"agent\":\"123\",\"parentTask\":\"12312123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"label\":\"123\",\"delFlag\":\"0\",\"priority\":\"123\",\"type\":\"123\",\"solveResults\":\"123\",\"createBy\":\"admin\",\"updateBy\":\"\",\"theme\":\"123\",\"id\":135,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"agent\":\"123\",\"createBy\":\"admin\",\"delFlag\":\"0\",\"expectedCompletionTime\":1609171200000,\"id\":135,\"label\":\"123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"parentTask\":\"12312123\",\"priority\":\"123\",\"solveResults\":\"123\",\"status\":\"0\",\"theme\":\"123\",\"type\":\"123\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:28:00');
INSERT INTO `sys_oper_log` VALUES (197, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"胡琴\",\"parentTask\":\"255\",\"originalEstimateTime\":\"1h\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"id\":102,\"status\":\"开始\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"id\":102,\"originalEstimateTime\":\"1h\",\"parentTask\":\"255\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"开始\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:35:15');
INSERT INTO `sys_oper_log` VALUES (198, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"parentTask\":\"312223123\",\"type\":\"312\",\"createBy\":\"admin\",\"theme\":\"12\",\"id\":136,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"createBy\":\"admin\",\"id\":136,\"parentTask\":\"312223123\",\"status\":\"0\",\"theme\":\"12\",\"type\":\"312\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:35:30');
INSERT INTO `sys_oper_log` VALUES (199, '代码生成模板组管理', 1, 'com.ruoyi.gen.controller.GenTemplateSchemeController.add()', 'POST', 1, 'admin', NULL, '/scheme', '192.168.0.140', '', '{\"description\":\"测试11\",\"id\":9,\"title\":\"测试\"}', '{\"code\":200,\"data\":{\"description\":\"测试11\",\"id\":9,\"title\":\"测试\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:37:34');
INSERT INTO `sys_oper_log` VALUES (200, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"胡琴\",\"parentTask\":\"2551111111\",\"originalEstimateTime\":\"1h\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"id\":102,\"status\":\"开始\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"id\":102,\"originalEstimateTime\":\"1h\",\"parentTask\":\"2551111111\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"开始\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:40:54');
INSERT INTO `sys_oper_log` VALUES (201, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"123\",\"parentTask\":\"123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"label\":\"123\",\"priority\":\"123\",\"type\":\"123\",\"solveResults\":\"123\",\"createBy\":\"admin\",\"theme\":\"123111111111\",\"id\":137,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"agent\":\"123\",\"createBy\":\"admin\",\"id\":137,\"label\":\"123\",\"module\":\"123\",\"originalEstimateTime\":\"123\",\"parentTask\":\"123\",\"priority\":\"123\",\"solveResults\":\"123\",\"status\":\"0\",\"theme\":\"123111111111\",\"type\":\"123\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:41:18');
INSERT INTO `sys_oper_log` VALUES (202, '测试用例', 2, 'com.ruoyi.gen.controller.TestCaseController.edit()', 'PUT', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"agent\":\"胡琴\",\"parentTask\":\"2551111111\",\"module\":\"12\",\"originalEstimateTime\":\"1h\",\"delFlag\":\"0\",\"priority\":\"重要\",\"type\":\"任务\",\"solveResults\":\"未解决\",\"createBy\":\"\",\"updateBy\":\"\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"id\":102,\"status\":\"开始\"}', '{\"code\":200,\"data\":{\"agent\":\"胡琴\",\"createBy\":\"\",\"delFlag\":\"0\",\"id\":102,\"module\":\"12\",\"originalEstimateTime\":\"1h\",\"parentTask\":\"2551111111\",\"priority\":\"重要\",\"solveResults\":\"未解决\",\"status\":\"开始\",\"theme\":\"测试用例-水印信息管理的数据修改\",\"type\":\"任务\",\"updateBy\":\"\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:57:51');
INSERT INTO `sys_oper_log` VALUES (203, '测试用例', 1, 'com.ruoyi.gen.controller.TestCaseController.add()', 'POST', 1, 'admin', NULL, '/case', '192.168.0.140', '', '{\"createBy\":\"admin\",\"theme\":\"封杀的\",\"id\":138,\"status\":\"0\"}', '{\"code\":200,\"data\":{\"createBy\":\"admin\",\"id\":138,\"status\":\"0\",\"theme\":\"封杀的\"},\"msg\":\"执行成功！\"}', 0, NULL, '2020-12-29 17:58:02');
INSERT INTO `sys_oper_log` VALUES (204, '数据源', 3, 'com.ruoyi.gen.controller.GeneratorDataSourceController.remove()', 'DELETE', 1, 'admin', NULL, '/source/5', '192.168.176.1', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2021-01-20 16:03:24');
INSERT INTO `sys_oper_log` VALUES (205, '代码生成模板组管理', 3, 'com.ruoyi.gen.controller.GenTemplateSchemeController.remove()', 'DELETE', 1, 'admin', NULL, '/scheme/9', '192.168.176.1', '', NULL, '{\"code\":200,\"msg\":\"执行成功\"}', 0, NULL, '2021-01-20 17:39:28');
INSERT INTO `sys_oper_log` VALUES (206, '代码生成模板组管理', 2, 'com.ruoyi.gen.controller.GenTemplateSchemeController.edit()', 'PUT', 1, 'admin', NULL, '/scheme', '127.0.0.1', '', '{\"description\":\"进行数据更新等等1\",\"id\":8,\"title\":\"增删改查001\"}', '{\"code\":200,\"data\":{\"description\":\"进行数据更新等等1\",\"id\":8,\"title\":\"增删改查001\"},\"msg\":\"执行成功！\"}', 0, NULL, '2021-01-21 10:41:01');
INSERT INTO `sys_oper_log` VALUES (207, '代码生成模板组管理', 2, 'com.ruoyi.gen.controller.GenTemplateSchemeController.edit()', 'PUT', 1, 'admin', NULL, '/scheme', '127.0.0.1', '', '{\"updateBy\":\"admin\",\"description\":\"进行数据更新等等111\",\"id\":8,\"title\":\"增删改查001\"}', '{\"code\":200,\"data\":{\"description\":\"进行数据更新等等111\",\"id\":8,\"title\":\"增删改查001\",\"updateBy\":\"admin\"},\"msg\":\"执行成功！\"}', 0, NULL, '2021-01-21 10:48:49');
INSERT INTO `sys_oper_log` VALUES (208, '代码生成模板组管理', 2, 'com.ruoyi.gen.controller.GenTemplateSchemeController.edit()', 'PUT', 1, 'admin', NULL, '/scheme', '127.0.0.1', '', '{\"updateBy\":\"admin\",\"description\":\"进行数据更新zrd\",\"updateTime\":1611197854099,\"id\":8,\"title\":\"增删改查001\"}', '{\"code\":200,\"data\":{\"description\":\"进行数据更新zrd\",\"id\":8,\"title\":\"增删改查001\",\"updateBy\":\"admin\",\"updateTime\":1611197854099},\"msg\":\"执行成功！\"}', 0, NULL, '2021-01-21 10:57:33');
INSERT INTO `sys_oper_log` VALUES (209, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '192.168.176.1', '', '{\"dbName\":\"sy\",\"description\":\"长航\",\"url\":\"jdbc:mysql://192.168.50.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":2,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"sy\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"长航\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":2,\"url\":\"jdbc:mysql://192.168.50.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2021-02-01 15:12:47');
INSERT INTO `sys_oper_log` VALUES (210, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '192.168.176.1', '', '{\"dbName\":\"sy\",\"description\":\"长航\",\"url\":\"jdbc:mysql://192.168.50.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":2,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"sy\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"长航\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":2,\"url\":\"jdbc:mysql://192.168.50.222:3308/sy?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2021-02-01 15:12:48');
INSERT INTO `sys_oper_log` VALUES (211, '数据源', 2, 'com.ruoyi.gen.controller.GeneratorDataSourceController.edit()', 'PUT', 1, 'admin', NULL, '/source', '192.168.176.1', '', '{\"dbName\":\"show\",\"description\":\"展会\",\"url\":\"jdbc:mysql://192.168.50.222:3308/show?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\",\"dbKey\":\"sy\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"dbUser\":\"root\",\"id\":3,\"dbPassword\":\"123456\"}', '{\"code\":200,\"data\":{\"dbKey\":\"sy\",\"dbName\":\"show\",\"dbPassword\":\"123456\",\"dbUser\":\"root\",\"description\":\"展会\",\"driverClass\":\"com.mysql.cj.jdbc.Driver\",\"id\":3,\"url\":\"jdbc:mysql://192.168.50.222:3308/show?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\"},\"msg\":\"执行成功！\"}', 0, NULL, '2021-02-01 15:12:48');
INSERT INTO `sys_oper_log` VALUES (212, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.176.1', '', '', '{\"msg\":\"文件服务异常，请联系管理员\",\"code\":500}', 0, NULL, '2021-02-03 11:01:09');
INSERT INTO `sys_oper_log` VALUES (213, '个人信息', 2, 'com.ruoyi.system.controller.SysProfileController.updateProfile()', 'PUT', 1, 'admin', NULL, '/user/profile', '192.168.176.1', '', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"phonenumber\":\"15888888888\",\"admin\":true,\"loginDate\":1521171180000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"password\":\"$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2\",\"loginIp\":\"127.0.0.1\",\"email\":\"ry@163.com\",\"nickName\":\"若依\",\"sex\":\"1\",\"deptId\":103,\"avatar\":\"http://192.168.50.222:4000/test/2020/12/29/60b552a4-b2b9-4422-b406-568e61ce8690.jpeg\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"createTime\":1521171180000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-02-03 11:01:44');
INSERT INTO `sys_oper_log` VALUES (214, '用户头像', 2, 'com.ruoyi.system.controller.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/user/profile/avatar', '192.168.176.1', '', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"http://192.168.50.222:4000/test/2021/02/03/6a468d76-59ea-44a3-b4f3-afd381776f20.jpeg\",\"code\":200}', 0, NULL, '2021-02-03 11:02:25');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-12-28 18:02:16', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);
INSERT INTO `sys_role_menu` VALUES (2, 1062);
INSERT INTO `sys_role_menu` VALUES (2, 1063);
INSERT INTO `sys_role_menu` VALUES (2, 1064);
INSERT INTO `sys_role_menu` VALUES (2, 1065);
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (2, 2009);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2020);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', 'http://192.168.50.222:4000/test/2021/02/03/6a468d76-59ea-44a3-b4f3-afd381776f20.jpeg', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'ry', '2021-02-03 11:01:42', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
