package com.ruoyi.gen.controller;

import cn.hutool.core.util.StrUtil;
import com.common.zrd.json.CommonJsonResult;
import com.common.zrd.mysql.MySqlInfo;
import com.common.zrd.validation.CommonValidation;
import com.common.zrd.validation.errorcode.BaseErrorCodes;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.gen.domain.GenTable;
import com.ruoyi.gen.domain.GenTableColumn;
import com.ruoyi.gen.domain.GenTemplate;
import com.ruoyi.gen.domain.GeneratorDataSource;
import com.ruoyi.gen.domain.entity.GenEntity;
import com.ruoyi.gen.service.IGenTableColumnService;
import com.ruoyi.gen.service.IGenTableService;
import com.ruoyi.gen.service.IGenTemplateService;
import com.ruoyi.gen.service.IGeneratorDataSourceService;
import com.ruoyi.gen.util.MysqlGenUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 * 
 * @author ruoyi
 */
@RequestMapping("/gen")
@RestController
public class GenController extends BaseController
{
    @Autowired
    private IGenTableService genTableService;
    @Autowired
    private IGenTemplateService genTemplateService;

    @Autowired
    private IGenTableColumnService genTableColumnService;

    @Autowired
    private IGeneratorDataSourceService generatorDataSourceService;
    /**
     * 查询代码生成列表
     */
    @PreAuthorize(hasPermi = "tool:gen:list")
    @GetMapping("/list")
    public TableDataInfo genList(GenTable genTable)
    {
        startPage();
        List<GenTable> list = genTableService.selectGenTableList(genTable);

        return getDataTable(list);
    }

    /**
     * 修改代码生成业务
     */
    @PreAuthorize(hasPermi = "tool:gen:query")
    @GetMapping(value = "/{talbleId}")
    public AjaxResult getInfo(@PathVariable Long talbleId)
    {
        GenTable table = genTableService.selectGenTableById(talbleId);
        List<GenTable> tables = genTableService.selectGenTableAll();
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(talbleId);
        Map<String, Object> map = new HashMap<>();
        map.put("info", table);
        map.put("rows", list);
        map.put("tables", tables);
        return AjaxResult.success(map);
    }

    /**
     * 查询数据库列表
     */
    @PreAuthorize(hasPermi = "tool:gen:list")
    @GetMapping("/db/list")
    public TableDataInfo dataList(GenTable genTable)
    {
        startPage();
        List<GenTable> list = genTableService.selectDbTableList(genTable);
        return getDataTable(list);
    }

    /**
     * 查询数据库列表
     */
    @PreAuthorize(hasPermi = "tool:gen:list" )
    @GetMapping("/db/listNew" )
    public CommonJsonResult dataListNew(GenTable genTable) throws SQLException, IOException, ClassNotFoundException {


        CommonValidation.isNull(genTable.getSourceId(), BaseErrorCodes.CHECK_ARG_NULL, "查询表", "数据源");
        GeneratorDataSource generatorDataSource = generatorDataSourceService.getById(genTable.getSourceId());
        List<GenTable> list = new ArrayList<>();
        MySqlInfo mySqlInfo = getMySqlInfoBySouceId(genTable.getSourceId());

        MysqlGenUtils mysql = new MysqlGenUtils(mySqlInfo);
        StringBuffer idsStr = new StringBuffer();
        idsStr.append("select table_name, table_comment, create_time, update_time from information_schema.tables" +
                " where table_schema = '" + generatorDataSource.getDbName() + "'");
        if (StrUtil.isNotEmpty(genTable.getTableName())) {
            idsStr.append("  AND lower(table_name) like lower(concat('%', '" + genTable.getTableName() + "', '%'))");
        }

        if (StrUtil.isNotEmpty(genTable.getTableComment())) {
            idsStr.append("  AND lower(table_comment) like lower(concat('%', '" + genTable.getTableComment() + "', " +
                    "'%'))");

        }
        String sql = idsStr.toString();
        list = mysql.getTablesInfo(sql);

        return CommonJsonResult.of(list);
    }

    /**
     * 描述：  通过数据源ID 获取jdbc连接
     * 备注：
     * 日期： 14:47 2020/12/25
     * 作者： zrd
     *
     * @param sourceId
     * @return com.common.zrd.mysql.MySqlInfo
     **/
    private MySqlInfo getMySqlInfoBySouceId(Long sourceId) {
        GeneratorDataSource generatorDataSource = generatorDataSourceService.getById(sourceId);
        MySqlInfo mySqlInfo = MysqlGenUtils.setMySqlInfo(generatorDataSource);
        return mySqlInfo;
    }
    /**
     * 查询数据表字段列表
     */
    @GetMapping(value = "/column/{talbleId}")
    public TableDataInfo columnList(Long tableId)
    {
        TableDataInfo dataInfo = new TableDataInfo();
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
        dataInfo.setRows(list);
        dataInfo.setTotal(list.size());
        return dataInfo;
    }

    /**
     * 导入表结构（保存）
     */
    @PreAuthorize(hasPermi = "tool:gen:import")
    @Log(title = "代码生成", businessType = BusinessType.IMPORT)
    @PostMapping("/importTableNew")
    public AjaxResult importTableSave(String tables) {
        String[] tableNames = Convert.toStrArray(tables);
        // 查询表信息
        List<GenTable> tableList = genTableService.selectDbTableListByNames(tableNames);
        genTableService.importGenTable(tableList);
        return AjaxResult.success();
    }

    /**
     * 导入表结构（保存）
     */
    @PreAuthorize(hasPermi = "tool:gen:list")
    @Log(title = "代码生成", businessType = BusinessType.IMPORT)
    @PutMapping("/importTable")
    public AjaxResult importTableSaveNew(@RequestBody GenEntity entity) throws SQLException, IOException,
            ClassNotFoundException {
        String[] tableNames = Convert.toStrArray(entity.getTables());
        CommonValidation.isNull(entity.getSourceId(), BaseErrorCodes.CHECK_ARG_NULL, "导入表", "数据源");
        CommonValidation.isNull(tableNames, BaseErrorCodes.CHECK_ARG_NULL, "导入表", "表名");
        GeneratorDataSource generatorDataSource = generatorDataSourceService.getById(entity.getSourceId());
        List<GenTable> list = new ArrayList<>();
        MySqlInfo mySqlInfo = MysqlGenUtils.setMySqlInfo(generatorDataSource);

        MysqlGenUtils mysql = new MysqlGenUtils(mySqlInfo);
        StringBuffer idsStr = new StringBuffer();
        idsStr.append("select table_name, table_comment, create_time, update_time from information_schema.tables" +
                " where table_schema = '" + generatorDataSource.getDbName() + "'");

        idsStr.append(" and table_name in ( ");
        for (int i = 0; i < tableNames.length; i++) {
            idsStr.append((i > 0) ? ",'" + tableNames[i] + "'" : "'" + tableNames[i] + "'");

        }
        idsStr.append(")");
        String sql = idsStr.toString();
//        List<GenTable> list11 = mysql.getTablesInfo(sql);

        // 查询表信息
        List<GenTable> tableList = mysql.getTablesInfo(sql);
        genTableService.importGenTableNew(generatorDataSource, tableList);
        return AjaxResult.success();
    }
    /**
     * 修改保存代码生成业务
     */
    @PreAuthorize(hasPermi = "tool:gen:edit")
    @Log(title = "代码生成", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult editSave(@Validated @RequestBody GenTable genTable)
    {
        genTableService.validateEdit(genTable);
        genTableService.updateGenTable(genTable);
        return AjaxResult.success();
    }

    /**
     * 删除代码生成
     */
    @PreAuthorize(hasPermi = "tool:gen:remove")
    @Log(title = "代码生成", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tableIds}")
    public AjaxResult remove(@PathVariable Long[] tableIds)
    {
        genTableService.deleteGenTableByIds(tableIds);
        return AjaxResult.success();
    }

    /**
     * 预览代码
     */
    @PreAuthorize(hasPermi = "tool:gen:preview")
    @GetMapping("/preview/{tableId}")
    public AjaxResult preview(@PathVariable("tableId") Long tableId) throws IOException
    {
        //1.获取表信息
        GenTable table = genTableService.selectGenTableById(tableId);
        //2.获取模板信息
        List<GenTemplate> templateList =
                genTemplateService.selectGenTemplateListBySid(Long.valueOf(table.getTplCategory()));
        Map<String, String> dataMap = genTableService.previewCodeNew(table, templateList);
        return AjaxResult.success(dataMap);
    }


    /**
     * 生成代码（下载方式）
     */
    @PreAuthorize(hasPermi = "tool:gen:code")
    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @GetMapping("/download/{tableName}")
    public void download(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException
    {
        byte[] data = genTableService.downloadCode(tableName);
        genCode(response, data);
    }

    /**
     * 生成代码（自定义路径）
     */
    @PreAuthorize(hasPermi = "tool:gen:code")
    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @GetMapping("/genCode/{tableName}")
    public AjaxResult genCode(@PathVariable("tableName") String tableName)
    {
        genTableService.generatorCode(tableName);
        return AjaxResult.success();
    }

    /**
     * 同步数据库
     */
    @PreAuthorize(hasPermi = "tool:gen:edit")
    @Log(title = "代码生成", businessType = BusinessType.UPDATE)
    @GetMapping("/synchDb/{tableName}")
    public AjaxResult synchDb(@PathVariable("tableName") String tableName)
    {
        genTableService.synchDb(tableName);
        return AjaxResult.success();
    }

    /**
     * 批量生成代码
     */
    @PreAuthorize(hasPermi = "tool:gen:code")
    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @GetMapping("/batchGenCode")
    public void batchGenCode(HttpServletResponse response, String tables) throws IOException
    {
        String[] tableNames = Convert.toStrArray(tables);
        byte[] data = genTableService.downloadCode(tableNames);
        genCode(response, data);
    }

    /**
     * 生成zip文件
     */
    private void genCode(HttpServletResponse response, byte[] data) throws IOException
    {
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }
}
